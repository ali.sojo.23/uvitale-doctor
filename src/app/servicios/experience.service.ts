import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";

@Injectable({
	providedIn: "root",
})
export class ExperienceService {
	constructor(private http: HttpClient) {}

	url = environment.urlHost;
	urlExperiences: string = this.url + "experiences/";

	create(data, identificador, token): Observable<any> {
		let json = {
			data: data,
			id: identificador,
		};
		let headers = new HttpHeaders()
			.set("Content-Type", "application/json")
			.set("access-token", token);
		return this.http.post(this.urlExperiences, json, { headers: headers });
	}
	get(identificador, token): Observable<any> {
		let headers = new HttpHeaders()
			.set("Content-Type", "application/json")
			.set("access-token", token);
		return this.http.get(this.urlExperiences + identificador, {
			headers: headers,
		});
	}
}
