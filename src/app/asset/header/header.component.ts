import { Component, OnInit, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { from } from "rxjs";
@Component({
	selector: "app-header",
	templateUrl: "./header.component.html",
	styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
	constructor(private __router: Router) {
		this.comprobate();
	}
	url: string = environment.urlHost;
	usuario: any = {};
	id: number;
	show: boolean = false;
	info: string;
	comprobate() {
		this.usuario = localStorage.getItem("import_data");
		this.info = localStorage.getItem("inf_tk");
		if (this.info == "undefined") {
			this.info = null;
		}
	}

	cerrarSesion() {
		localStorage.removeItem("tk_init");
		localStorage.removeItem("import_data");

		this.__router.navigate(["/login"]);
	}

	changeInformation() {
		window.location.reload();
	}
	ngOnInit() {
		this.comprobate();
	}
}
