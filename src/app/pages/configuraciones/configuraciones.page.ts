import {
	Component,
	OnInit,
	AfterContentChecked,
	OnDestroy,
} from "@angular/core";
import { UsuariosService } from "../../servicios/usuarios.service";
import { ActivatedRoute } from "@angular/router";
import { EstudiosService } from "../../servicios/estudios.service";
import { ExperienceService } from "../../servicios/experience.service";
import { AuthService } from "../../servicios/auth.service";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "../../../environments/environment";
import { LoadingController, AlertController } from "@ionic/angular";

@Component({
	selector: "app-configuraciones",
	templateUrl: "./configuraciones.page.html",
	styleUrls: ["./configuraciones.page.scss"],
})
export class ConfiguracionesPage
	implements OnInit, AfterContentChecked, OnDestroy {
	constructor(
		private __usuario: UsuariosService,
		private __route: ActivatedRoute,
		private __estudios: EstudiosService,
		private __exp: ExperienceService,
		private __auth: AuthService,
		private loader: LoadingController,
		private __alert: AlertController,
		private traductor: TranslateService
	) {
		this.obtenerUsuario();
		this.setLang();
	}
	url = environment.urlHost;
	usuario: any = {
		FirstName: "",
		LastName: "",
		email: "",
	};
	experiencia: any;
	estudios: any;
	identificador: string;
	Token: string;

	ngOnInit() {
		this.obtenerUsuario();
		this.urlSlug();
		this.token();
	}
	ngAfterContentChecked() {}

	token() {
		this.Token = localStorage.getItem("tk_init");
	}
	urlSlug() {
		this.identificador = this.__route.snapshot.paramMap.get("id");
	}

	doRefresh(event) {
		this.obtenerUsuario();
		setTimeout(() => {
			event.target.complete();
		}, 2000);
	}

	async obtenerUsuario() {
		let load = await this.loader.create({ message: "por favor espere" });
		let Alert = await this.__alert.create({
			header: "Alerta!!",
			message:
				"Error al conectarse al descargar la información del servidor",
			buttons: [
				{
					text: "Reload",
					role: "cancel",
					cssClass: "secondary",
					handler: () => {
						this.obtenerUsuario();
					},
				},
			],
		});

		load.present();
		Alert.dismiss();
		this.urlSlug();
		this.token();
		this.__usuario.mostrarUsuario(this.identificador, this.Token).subscribe(
			(resultado) => {
				load.dismiss();
				this.usuario = resultado.user;
				console.log(this.usuario);
			},
			(error) => {
				load.dismiss();
				Alert.present();
				console.log(JSON.stringify(error));
			}
		);
	}

	obtenesEstudios(identificador) {
		this.token();
		this.__estudios.getEstudios(identificador, this.Token).subscribe(
			(resultado) => {
				this.estudios = resultado.studies;
				console.log(this.estudios);
			},
			(error) => {
				console.log(error);
			}
		);
	}
	getExperiencia(identificador) {
		this.token();
		this.__exp.get(identificador, this.Token).subscribe(
			(resultado) => {
				this.experiencia = resultado;
			},
			(error) => {
				console.log(error);
			}
		);
	}
	async VerificarCorreo(identificador) {
		let Alert = await this.__alert.create({
			header: "",
			message: "Enviando correo de verificación de usuario",
			buttons: [
				{
					text: "Ok",
					role: "aubmit",
					cssClass: "secondary",
				},
			],
		});
		Alert.present();
		this.token();
		this.__auth.verifyEmail(identificador, this.token).subscribe(
			(resultado) => {
				console.log(resultado);
				this.usuario.email_verified_at = "verified";
			},
			(error) => {
				console.log(error);
			}
		);
	}

	setLang() {
		this.traductor.setDefaultLang("en");
		let lang = localStorage.getItem("prefer_lang");
		this.traductor.use(lang);
	}

	ngOnDestroy() {}
}
