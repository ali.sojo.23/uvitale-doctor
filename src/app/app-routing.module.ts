import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./Guard/auth.guard";

const routes: Routes = [
	{ path: "", redirectTo: "dashboard", pathMatch: "full" },
	{
		path: "dashboard",
		loadChildren: "./pages/home/home.module#HomePageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "notifications",
		loadChildren:
			"./pages/notifications/notifications.module#NotificationsPageModule",
	},
	{
		path: "perfil/:id",
		loadChildren:
			"./pages/configuraciones/configuraciones.module#ConfiguracionesPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "doctors",
		loadChildren: "./pages/doctors/doctors.module#DoctorsPageModule",
	},
	{
		path: "notify-chats",
		loadChildren:
			"./pages/notify-chats/notify-chats.module#NotifyChatsPageModule",
	},
	{
		path: "perfil/editar/:id",
		loadChildren:
			"./pages/editar-mi-perfil/editar-mi-perfil.module#EditarMiPerfilPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "login",
		loadChildren: "./pages/login/login.module#LoginPageModule",
	},
	{
		path: "register",
		loadChildren: "./pages/register/register.module#RegisterPageModule",
	},
	{
		path: "forgot-password",
		loadChildren:
			"./pages/forgot-password/forgot-password.module#ForgotPasswordPageModule",
	},
	{
		path: "estudios",
		loadChildren:
			"./pages/configuraciones/asset/estudios/estudios.module#EstudiosPageModule",
	},
	{
		path: "trabajos",
		loadChildren:
			"./pages/configuraciones/asset/trabajos/trabajos.module#TrabajosPageModule",
	},
	{ path: "maps", loadChildren: "./pages/maps/maps.module#MapsPageModule" },
	{
		path: "mobile-menu",
		loadChildren:
			"./pages/mobile-menu/mobile-menu.module#MobileMenuPageModule",
	},
	{ path: "chat", loadChildren: "./pages/chat/chat.module#ChatPageModule" },
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
